package com.awilson.sample.analytics;

import android.content.Context;
import android.text.TextUtils;

import com.google.analytics.tracking.android.ExceptionReporter;
import com.google.analytics.tracking.android.Fields;
import com.google.analytics.tracking.android.GAServiceManager;
import com.google.analytics.tracking.android.GoogleAnalytics;
import com.google.inject.Inject;
import com.google.inject.name.Named;
import com.awilson.sample.analytics.model.Buildable;

import java.util.Map;

public class TrackerImpl implements Tracker {

    private com.google.analytics.tracking.android.Tracker tracker;
	private final String GOOGLE_ANALYTICS_TRACKER_ID = "XXXXXXXXXXXXX";

    @Inject
    /* package */ TrackerImpl(Context context, GoogleAnalytics googleAnalytics) {
        this.tracker = googleAnalytics.getTracker(GOOGLE_ANALYTICS_TRACKER_ID);
        setUncaughtExceptionReporting(context);
    }

    @Override
    public void send(Buildable buildable) {
        Map<String, String> map = buildable.build();
        if (map != null) {
            this.tracker.send(map);
        }
    }

    @Override
    public String get(String key) {
        return this.tracker.get(key);
    }

    @Override
    public void set(String key, String value) {
        this.tracker.set(key, value);
    }

    @Override
    public void setCustomDimension(String value) {
        if (TextUtils.isEmpty(value)) value = "Unknown";
        this.tracker.set(Fields.customDimension(1), value);
    }

    private void setUncaughtExceptionReporting(Context context) {
        Thread.UncaughtExceptionHandler uncaughtExceptionHandler =
                new ExceptionReporter(this.tracker,
                        GAServiceManager.getInstance(),
                        Thread.getDefaultUncaughtExceptionHandler(),
                        context);
        
        Thread.setDefaultUncaughtExceptionHandler(uncaughtExceptionHandler);
    }
}