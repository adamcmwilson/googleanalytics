package com.awilson.sample.analytics;

import android.app.Application;

import com.google.analytics.tracking.android.GoogleAnalytics;
import com.google.analytics.tracking.android.Logger;
import com.google.inject.Inject;
import com.google.inject.Provider;
import com.google.inject.Singleton;

@Singleton
public class GoogleAnalyticsProvider implements Provider<GoogleAnalytics> {

    private final Application application;

    @Inject
    /* package */ GoogleAnalyticsProvider(Application application) {
        this.application = application;
    }

    @Override
    public GoogleAnalytics get() {
        GoogleAnalytics googleAnalytics = GoogleAnalytics.getInstance(application.getApplicationContext());
        googleAnalytics.getLogger().setLogLevel(Logger.LogLevel.VERBOSE);

        return googleAnalytics;
    }

}