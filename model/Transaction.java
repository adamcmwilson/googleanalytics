package com.awilson.sample.analytics.model;

import android.text.TextUtils;

import com.google.analytics.tracking.android.MapBuilder;

import java.util.Map;

public class Transaction implements Buildable {

    public final String transactionId;
    public final String affiliation;
    public final String currencyCode;
    public final double revenue;
    public final double tax;
    public final double shipping;

    public Transaction(String transactionId, String affiliation, double revenue, double tax, double shipping, String currencyCode) {
        this.transactionId = transactionId;
        this.affiliation = affiliation;
        this.revenue = revenue;
        this.tax = tax;
        this.shipping = shipping;
        this.currencyCode = currencyCode;
    }

    @Override
    public Map<String, String> build() {
        if (TextUtils.isEmpty(transactionId) || TextUtils.isEmpty(affiliation) || TextUtils.isEmpty(currencyCode)) return null;
        else return MapBuilder.createTransaction(transactionId, affiliation, revenue, tax, shipping, currencyCode).build();
    }

    @Override
    public String toString() {
        return this.getClass().getSimpleName()
                .concat(" | ").concat(transactionId)
                .concat(" | ").concat(affiliation)
                .concat(" | ").concat(currencyCode)
                .concat(" | ").concat(String.valueOf(revenue))
                .concat(" | ").concat(String.valueOf(tax))
                .concat(" | ").concat(String.valueOf(shipping));
    }

}