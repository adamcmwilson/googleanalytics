package com.awilson.sample.analytics.model;

import android.text.TextUtils;

import com.google.analytics.tracking.android.MapBuilder;

import java.util.Map;

public class Event implements Buildable {

    public final String category;
    public final String action;
    public final String label;
    public final Long value;

    public Event(String category, String action, String label, Long value) {
        this.category = category;
        this.action = action;
        this.label = label;
        this.value = value;
    }

    @Override
    public Map<String, String> build() {
        if (TextUtils.isEmpty(category) || TextUtils.isEmpty(action) || TextUtils.isEmpty(label)) return null;
        else return MapBuilder.createEvent(category, action, label, value).build();
    }

    @Override
    public String toString() {
        return this.getClass().getSimpleName()
                .concat(" | ").concat(category)
                .concat(" | ").concat(action)
                .concat(" | ").concat(label)
                .concat(" | ").concat(String.valueOf(value));
    }
}