package com.awilson.sample.analytics.model;

import android.text.TextUtils;

import com.google.analytics.tracking.android.MapBuilder;

import java.util.Map;

public class Social implements Buildable {
    public final String network;
    public final String action;
    public final String target;

    public Social(String network, String action, String target) {
        this.network = network;
        this.action = action;
        this.target = target;
    }

    @Override
    public Map<String, String> build() {
        if (TextUtils.isEmpty(network) || TextUtils.isEmpty(action) || TextUtils.isEmpty(target)) return null;
        else return MapBuilder.createSocial(network, action, target).build();
    }

    @Override
    public String toString() {
        return this.getClass().getSimpleName()
                .concat(" | ").concat(network)
                .concat(" | ").concat(action)
                .concat(" | ").concat(target);
    }

}