package com.awilson.sample.analytics.model;

public class AnalyticsScreenName {

    // NOTE: CHANGING THESE VALUES WILL RESULT IN POOR ANALYTICS DATA QUALITY
    // DO NOT CHANGE THE VALUES UNLESS YOU ARE SURE IT IS CORRECT TO DO SO

    public static final String ABOUT = "About Screen";
    public static final String LOGIN = "Login Screen";
    public static final String SETTINGS = "Settings Screen";
}