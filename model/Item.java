package com.awilson.sample.analytics.model;

import android.text.TextUtils;

import com.google.analytics.tracking.android.MapBuilder;

import java.util.Map;

public class Item implements Buildable {

    private final String transactionId;
    private final String name;
    private final String sku;
    private final String category;
    private final double price;
    private final long quantity;
    private final String currencyCode;

    public Item(String transactionId, String name, String sku, String category, double price, long quantity, String currencyCode) {
        this.transactionId = transactionId;
        this.name = name;
        this.sku = sku;
        this.category = category;
        this.price = price;
        this.quantity = quantity;
        this.currencyCode = currencyCode;
    }

    @Override
    public Map<String, String> build() {
        if (TextUtils.isEmpty(transactionId) || TextUtils.isEmpty(name) || TextUtils.isEmpty(sku)
                || TextUtils.isEmpty(category) || TextUtils.isEmpty(currencyCode)) return null;
        else return MapBuilder.createItem(transactionId, name, sku, category, price, quantity, currencyCode).build();
    }

    @Override
    public String toString() {
        return this.getClass().getSimpleName()
                .concat(" | ").concat(transactionId)
                .concat(" | ").concat(name)
                .concat(" | ").concat(sku)
                .concat(" | ").concat(category)
                .concat(" | ").concat(String.valueOf(price))
                .concat(" | ").concat(String.valueOf(quantity))
                .concat(" | ").concat(currencyCode);
    }

}