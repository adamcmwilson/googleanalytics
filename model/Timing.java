package com.awilson.sample.analytics.model;

import android.text.TextUtils;

import com.google.analytics.tracking.android.MapBuilder;

import java.util.Map;

public class Timing implements Buildable {

    private final String category;
    private final Long intervalInMilliseconds;
    private final String name;
    private final String label;

    public Timing(String category, Long intervalInMilliseconds, String name, String label) {
        this.category = category;
        this.intervalInMilliseconds = intervalInMilliseconds;
        this.name = name;
        this.label = label;
    }

    @Override
    public Map<String, String> build() {
        if (TextUtils.isEmpty(category)
                || TextUtils.isEmpty(name)
                || TextUtils.isEmpty(name)
                || TextUtils.isEmpty(label))
            return null;
        else return MapBuilder.createTiming(this.category, this.intervalInMilliseconds, this.name, this.label).build();
    }

    @Override
    public String toString() {
        return this.getClass().getSimpleName()
                .concat(" | ").concat(category)
                .concat(" | ").concat(String.valueOf(intervalInMilliseconds))
                .concat(" | ").concat(name)
                .concat(" | ").concat(label);
    }
}