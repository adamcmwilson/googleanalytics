package com.awilson.sample.analytics.model;

import java.util.Map;

public interface Buildable {

    Map<String, String> build();

}