package com.awilson.sample.analytics.model;

import android.text.TextUtils;

import com.google.analytics.tracking.android.Fields;
import com.google.analytics.tracking.android.MapBuilder;

import java.util.Map;

public class AppView implements Buildable {

    public final String screenName;

    public AppView(String screenName) {
        this.screenName = screenName;
    }

    @Override
    public Map<String, String> build() {
        if (TextUtils.isEmpty(this.screenName)) return null;
        else return MapBuilder.createAppView().set(Fields.SCREEN_NAME, screenName).build();
    }

    @Override
    public String toString() {
        return this.getClass().getSimpleName().concat(" | ").concat(screenName);
    }
}