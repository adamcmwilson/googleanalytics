package com.awilson.sample.analytics.model;

import android.text.TextUtils;

import com.google.analytics.tracking.android.MapBuilder;

import java.util.Map;

public class Exception implements Buildable {

    public final java.lang.String exceptionDescription;
    public final java.lang.Boolean fatal;

    public Exception(String exceptionDescription, Boolean fatal) {
        this.exceptionDescription = exceptionDescription;
        this.fatal = fatal;
    }

    @Override
    public Map<String, String> build() {
        if (TextUtils.isEmpty(this.exceptionDescription)) return null;
        else return MapBuilder.createException(this.exceptionDescription, this.fatal).build();
    }

    @Override
    public String toString() {
        return this.getClass().getSimpleName()
                .concat(" | ").concat(exceptionDescription)
                .concat(" | ").concat(String.valueOf(fatal));
    }
}