package com.awilson.sample.analytics;

import com.awilson.sample.analytics.model.Buildable;

public interface Tracker {

    public void send(Buildable buildable);

    public java.lang.String get(java.lang.String key);

    public void set(java.lang.String key, java.lang.String value);

    public void setCustomDimension(String value);

}